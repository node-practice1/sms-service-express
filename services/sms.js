const TrezSmsClient = require("trez-sms-client");

const username = "myusername";
const password = "mypassword";
const client  = new TrezSmsClient(username,password) ;

function sendSms(phoneNumber)
{
    client.manualSendCode(phoneNumber , "Test Message")
    .then((messageId) => {
        console.log("Sent Message ID: " + messageId);
    })
    .catch(error => console.log(error));
}
module.exports = sendSms;