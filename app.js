const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");

const homeRoutes = require("./routes/home.js");
const adminRoutes = require("./routes/admin.js");
const rootDir = require("./utils/path.js");

const app = express();

app.use(bodyParser.urlencoded({extended:false}));

app.use("/" ,homeRoutes);
app.use("/admin" ,adminRoutes);

app.use((req,res) => {
    res.status(404).sendFile(path.join(rootDir,"views" , "404.html"));
})


app.listen(4200,() =>{console.log("Server is Running on Port 4200")});