const{Router} = require("express");
const fs = require("fs");
const path = require("path");
const sendSms = require("../services/sms");
const rootDir = require("../utils/path.js")

const router = new Router();

router.post("/save" , (req ,res) =>{

    let {phone_number} = req.body;
    fs.writeFile(path.join(rootDir,"data","phone-number.txt"),phone_number,(err) =>{
        if(err)
        {
            throw err
        }
        else{
            console.log("has been writed");
        }
        res.redirect("/home");
    });
})
router.get("/send" , (req, res) => {

    fs.readFile(path.join(rootDir,"data" ,"phone-number.txt"),(err,data) =>{
        if(err)
        {
            console.log(err)
        }
        else{

            let phone_number = data.toString();
            sendSms(phone_number);
        }
    })
    res.redirect("/home");
})

module.exports = router;