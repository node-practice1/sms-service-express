const{Router} = require("express");4
const rootDir = require("../utils/path")
const path = require("path");

const router = new Router();

router.get("/home" , (req,res) =>{
    res.sendFile(path.join(rootDir,"views" ,"home.html"));
})

module.exports = router;